# build from python 2.7 with alpine(small linux)
FROM python:2.7-alpine
# Install Pytest as well

RUN pip install -U pytest

#Copy code
COPY ./src /code
WORKDIR /code

ENTRYPOINT pytest --verbose --junit-xml test-reports/results.xml
